=======================
python-storyboardclient
=======================

Python Client library for StoryBoard

This is the StoryBoard python client! It lets you interact with
StoryBoard from the comfort of your own terminal! There is no
command to run this; instead you can import it into scripts. This
lets you perform complex actions on things in StoryBoard, eg: add an
helpful comment on all stories with 'cannot store contact information'
in the description, pointing users at the relevant docs, but only
if there is no comment to this effect already. (There is an example
of such a script in the documentation.)

Happy task-tracking!

* Free software: Apache license
* Documentation: http://docs.openstack.org/infra/storyboard/
* Source: http://git.openstack.org/cgit/openstack-infra/python-storyboardclient
* Bugs: https://storyboard.openstack.org/#!/project/755
